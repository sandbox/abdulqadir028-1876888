<?php

/**
 * @file
 * Processor functions for the feed_loader module.
 */

/**
 * Implements hook_feed_loader_process_info().
 */
function feed_loader_feed_loader_process_info() {
  return array(
    'title' => t('Default processor'),
    'description' => t('Creates lightweight records from feed items.'),
  );
}

/**
 * Implements hook_feed_loader_process().
 */
function feed_loader_feed_loader_process($feed) {
  if (is_object($feed)) {
    if (is_array($feed->items)) {
      foreach ($feed->items as $item) {
        // Save this item. Try to avoid duplicate entries as much as possible. If
        // we find a duplicate entry, we resolve it and pass along its ID is such
        // that we can update it if needed.
        if (!empty($item['guid'])) {
          $entry = db_query("SELECT iid, timestamp FROM {feed_loader_item} WHERE fid = :fid AND guid = :guid", array(':fid' => $feed->fid, ':guid' => $item['guid']))->fetchObject();
        }
        elseif ($item['link'] && $item['link'] != $feed->link && $item['link'] != $feed->url) {
          $entry = db_query("SELECT iid, timestamp FROM {feed_loader_item} WHERE fid = :fid AND link = :link", array(':fid' => $feed->fid, ':link' => $item['link']))->fetchObject();
        }
        else {
          $entry = db_query("SELECT iid, timestamp FROM {feed_loader_item} WHERE fid = :fid AND title = :title", array(':fid' => $feed->fid, ':title' => $item['title']))->fetchObject();
        }
        if (!$item['timestamp']) {
          $item['timestamp'] = isset($entry->timestamp) ? $entry->timestamp : REQUEST_TIME;
        }

        // Make sure the item title and author fit in the 255 varchar column.
        $item['title'] = truncate_utf8($item['title'], 255, TRUE, TRUE);
        $item['author'] = truncate_utf8($item['author'], 255, TRUE, TRUE);
        feed_loader_save_item(array('iid' => (isset($entry->iid) ? $entry->iid : ''), 'fid' => $feed->fid, 'timestamp' => $item['timestamp'], 'title' => $item['title'], 'link' => $item['link'], 'author' => $item['author'], 'description' => $item['description'], 'guid' => $item['guid']));
      }
    }
  }
}

/**
 * Implements hook_feed_loader_remove().
 */
function feed_loader_feed_loader_remove($feed) {
  $iids = db_query('SELECT iid FROM {feed_loader_item} WHERE fid = :fid', array(':fid' => $feed->fid))->fetchCol();
  if ($iids) {
    db_delete('feed_loader_category_item')
      ->condition('iid', $iids, 'IN')
      ->execute();
  }
  db_delete('feed_loader_item')
    ->condition('fid', $feed->fid)
    ->execute();

  drupal_set_message(t('The news items from %site have been removed.', array('%site' => $feed->title)));
}

/**
 * Implements hook_form_feed_loader_admin_form_alter().
 *
 * Form alter feed_loader module's own form to keep processor functionality
 * separate from feed_loader API functionality.
 */
function feed_loader_form_feed_loader_admin_form_alter(&$form, $form_state) {
  if (in_array('feed_loader', variable_get('feed_loader_processors', array('feed_loader')))) {
    $info = module_invoke('feed_loader', 'feed_loader_process', 'info');
    $items = drupal_map_assoc(array(3, 5, 10, 15, 20, 25), '_feed_loader_items');
    $period = drupal_map_assoc(array(3600, 10800, 21600, 32400, 43200, 86400, 172800, 259200, 604800, 1209600, 2419200, 4838400, 9676800), 'format_interval');
    $period[FEEDLOADER_CLEAR_NEVER] = t('Never');

    // Only wrap into a collapsible fieldset if there is a basic configuration.
    if (isset($form['basic_conf'])) {
      $form['modules']['feed_loader'] = array(
        '#type' => 'fieldset',
        '#title' => t('Default processor settings'),
        '#description' => check_plain($info['description']),
        '#collapsible' => TRUE,
        '#collapsed' => !in_array('feed_loader', variable_get('feed_loader_processors', array('feed_loader'))),
      );
    }
    else {
      $form['modules']['feed_loader'] = array();
    }

    $form['modules']['feed_loader']['feed_loader_summary_items'] = array(
      '#type' => 'select',
      '#title' => t('Number of items shown in listing pages'),
      '#default_value' => variable_get('feed_loader_summary_items', 3),
      '#empty_value' => 0,
      '#options' => $items,
    );

    $form['modules']['feed_loader']['feed_loader_clear'] = array(
      '#type' => 'select',
      '#title' => t('Discard items older than'),
      '#default_value' => variable_get('feed_loader_clear', 9676800),
      '#options' => $period,
      '#description' => t('Requires a correctly configured <a href="@cron">cron maintenance task</a>.', array('@cron' => url('admin/reports/status'))),
    );

    $form['modules']['feed_loader']['feed_loader_category_selector'] = array(
      '#type' => 'radios',
      '#title' => t('Select categories using'),
      '#default_value' => variable_get('feed_loader_category_selector', 'checkboxes'),
      '#options' => array('checkboxes' => t('checkboxes'),
      'select' => t('multiple selector')),
      '#description' => t('For a small number of categories, checkboxes are easier to use, while a multiple selector works well with large numbers of categories.'),
    );
    $form['modules']['feed_loader']['feed_loader_teaser_length'] = array(
      '#type' => 'select',
      '#title' => t('Length of trimmed description'),
      '#default_value' => variable_get('feed_loader_teaser_length', 600),
      '#options' => drupal_map_assoc(array(0, 200, 400, 600, 800, 1000, 1200, 1400, 1600, 1800, 2000), '_feed_loader_characters'),
      '#description' => t("The maximum number of characters used in the trimmed version of content.")
    );

  }
}

/**
 * Creates display text for teaser length option values.
 *
 * Callback for drupal_map_assoc() within
 * feed_loader_form_feed_loader_admin_form_alter().
 */
function _feed_loader_characters($length) {
  return ($length == 0) ? t('Unlimited') : format_plural($length, '1 character', '@count characters');
}

/**
 * Adds/edits/deletes an feed_loader item.
 *
 * @param $edit
 *   An associative array describing the item to be added/edited/deleted.
 */
function feed_loader_save_item($edit) {
  $lnk = explode('/', $edit['guid']);
  if (!empty($lnk[2])) {
    $link = $lnk[2];
  }
  $patterns = array();
  $remove = array('@<script[^>]*?>.*?</script>@si',  // Strip out javascript 
                  '@<[\/\!]*?[^<>]*?>@si',            // Strip out HTML tags 
                  '@<style[^>]*?>.*?</style>@siU',    // Strip style tags properly 
                  '@<![\s\S]*?--[ \t\n\r]*>@'         // Strip multi-line comments including CDATA 
  ); 
  $text3 = '"/sites/all/modules/feed_loader/images/feed.png" alt="Default" border="1" width="50" height="50"';
  $text1 = explode("<img src=", $edit['description']);
  if (!empty($text1[1])) {
    $text2 = explode("/>", $text1[1]);
  //$text3 = $text1[1];
  }
  if (!empty($text2[0])) {
    $text3 = $text2[0];
  }
  //$edit['description'] = preg_replace($remove, ' ', $edit['description']); 
  if ($edit['title'] && empty($edit['iid'])) {
    $edit['iid'] = db_insert('feed_loader_item')
    ->fields(array(
    'title' => $edit['title'],
    'link' => $edit['link'],
    'author' => $edit['author'],
    'description' => preg_replace($remove, ' ', $edit['description']),
    'description_html' => $edit['description'],
    'image' => "<img src=" . $text3 . "/>",
    'guid' => $edit['guid'],
    'origin' => $link,
    'timestamp' => $edit['timestamp'],
    'fid' => $edit['fid'],
    ))
    ->execute();
  }
  if ($edit['iid'] && !$edit['title']) {
    db_delete('feed_loader_item')
      ->condition('iid', $edit['iid'])
      ->execute();
    db_delete('feed_loader_category_item')
      ->condition('iid', $edit['iid'])
      ->execute();
  }
  elseif ($edit['title'] && $edit['link']) {
    // file the items in the categories indicated by the feed
    $result = db_query('SELECT cid FROM {feed_loader_category_feed} WHERE fid = :fid', array(':fid' => $edit['fid']));
    foreach ($result as $category) {
      db_merge('feed_loader_category_item')
        ->key(array(
          'iid' => $edit['iid'],
          'cid' => $category->cid,
        ))
        ->execute();
    }
  }
}

/**
 * Expires items from a feed depending on expiration settings.
 *
 * @param $feed
 *   Object describing feed.
 */
function feed_loader_expire($feed) {
  $feed_loader_clear = variable_get('feed_loader_clear', 9676800);

  if ($feed_loader_clear != FEEDLOADER_CLEAR_NEVER) {
    // Remove all items that are older than flush item timer.
    $age = REQUEST_TIME - $feed_loader_clear;
    $iids = db_query('SELECT iid FROM {feed_loader_item} WHERE fid = :fid AND timestamp < :timestamp', array(
      ':fid' => $feed->fid,
      ':timestamp' => $age,
    ))
    ->fetchCol();
    if ($iids) {
      db_delete('feed_loader_category_item')
        ->condition('iid', $iids, 'IN')
        ->execute();
      db_delete('feed_loader_item')
        ->condition('iid', $iids, 'IN')
        ->execute();
    }
  }
}
