<?php

/**
 * @file
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 * Abdul@drupalmate.com
 */
?>
<style>
div.feeds {
	width:720px;
	padding:10px;
	border:5px solid gray;
	margin:0px;
}
div.title {
	width:680px;
	padding-bottom:0px;
}
div.body {
	width:590px;
	padding-top:5px;
	padding-left:100px;
	margin-top:-80px;
	min-height:100px;
}
div.image {
	width:100px;
	padding-right:10px;
	padding-top:15px;
	padding-bottom:10px;
}
div.date {
	width:150px;
	padding-left:0px;
	padding-top:5px;
	margin-left:500px;
	font-size:12px;	
}
div.origin {
	width:590px;
	padding-left:0px;
	margin-top:-15px;
}
</style>
<?php
global $user;	
global $base_path;	
$image = $row->feed_loader_item_feed_loader_node_feed_image;
if(strlen($image)>10){
	$image = $image;
	}
else if($image==0){
	$image = '<img class="image_left" src="/sites/all/modules/feed_loader/images/feed.png" alt="Default" border="1" width="50" height="50">';
}
else {
	$user = user_load($image);
	$image = '<img src="'.$base_path.'/sites/default/files/styles/thumbnail/public/pictures/'.$user->picture->filename.'" alt="Default" border="1" width="80" height="80">';
	}
if($row->feed_loader_item_feed_loader_node_feed_origin == "User Comment"){
	$origin_link = '#';
	$origin_a_tag = "via <a href='".$origin_link."' >";
}
else{
	$origin_link = $row->feed_loader_item_feed_loader_node_feed_origin;
	$origin_a_tag = "via <a href='http://".$origin_link."' target='_blank'>";
}
?> 
	<div class=content>
    <div class="feeds"> 
    <div class="title"><?php print "<h1><a href='".$row->feed_loader_item_feed_loader_node_feed_link."' target='_blank'>".$row->feed_loader_item_feed_loader_node_feed_title."</a></h1>";?></div>
    <div class="image"><?php print $image; ?> </div>
    <div class="body"><?php print $row->feed_loader_item_feed_loader_node_feed_description; ?> </div>
    <div class="date"><?php print date("d-M-Y H:i:s",$row->feed_loader_item_feed_loader_node_feed_timestamp);?></div>
    <div class="origin"><?php print $origin_a_tag.$row->feed_loader_item_feed_loader_node_feed_origin."</a>";?></div>
	<?php //print_r($user); ?>
   
    </div>
    <?php print "<br>";?>
	</div>
