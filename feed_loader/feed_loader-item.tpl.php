<?php

/**
 * @file
 * Default theme implementation to format an individual feed item for display
 * on the aggregator page.
 *
 * Available variables:
 * - $feed_url: URL to the originating feed item.
 * - $feed_title: Title of the feed item.
 * - $source_url: Link to the local source section.
 * - $source_title: Title of the remote source.
 * - $origin: The Origin of the Feed
 * - $author: The Author if the Feed is a User Comment
 * - $source_date: Date the feed was posted on the remote source.
 * - $content: Feed item content in HTML.
 * - $description: Feed Description withot HTML
 * - $image: Feed Image from the Origin
 * - $categories: Linked categories assigned to the feed.
 *
 * @see template_preprocess()
 * @see template_preprocess_feed_loader_item()
 *
 * @ingroup themeable
 */
?>
<div class="feed-item">
  <h3 class="feed-item-title">
    <a href="<?php print $feed_url; ?>"><?php print $feed_title; ?></a>
  </h3>

  <div class="feed-item-meta">
  <?php if ($source_url): ?>
    <a href="<?php print $source_url; ?>" class="feed-item-source"><?php print $source_title; ?></a> -
  <?php endif; ?>
    <span class="feed-item-date"><?php print $source_date; ?></span>
  </div>

<?php if ($content): ?>
  <div class="feed-item-body">
    <?php print $content; ?>
    <?php 
    if ($origin != 'User Comment') :
        print '<br /><br />' . '<a href="http://' . $origin . '" class="feed-item-source" target="_blank">' . $origin . '</a>';
    // }
    else :
        global $base_path;
        print '<br /><br />' . '<a href="' . $base_path . 'users/' . $author . '" class="feed-item-source" target="_blank">' . $author . '</a>';
    // }
    endif;
    ?>
  </div>
<?php endif; ?>

<?php if ($categories): ?>
  <div class="feed-item-categories">
    <?php print t('Categories'); ?>: <?php print implode(', ', $categories); ?>
  </div>
<?php endif;?>

</div>
