<?php

/**
 * @file
 * Views functions for the feed_loader module.
 */

/**
 * Implement hook_views_data()
 * Abdul@drupalmate.com
 * This will allow to create a view with this content
 *
 */
 
function feed_loader_views_data() {

  $data['feed_loader_item']['table']['group'] = t('Feeds_Items');

  $data['feed_loader_item']['table']['base'] = array(
    'field' => 'iid', // This is the identifier field for the view. 
    'title' => t('Feeds_Items'), 
    'help' => t('Feed Loader Item table contains feed content and can be related to nodes.'), 
    'weight' => -10,
  );
  
  $data['feed_loader_node_feed']['table']['group'] = t('Feeds');

  $data['feed_loader_node_feed']['table']['base'] = array(
    'field' => 'fid', // This is the identifier field for the view. 
    'title' => t('Feeds'), 
    'help' => t('Feed Loader Feeds Node table contains feed content and can be related to nodes.'), 
    'weight' => -10,
  );

  $data['feed_loader_item']['table']['join'] = array(
    'feed_loader_node_feed' => array(
      'left_field' => 'fid', 
      'field' => 'fid',
    ),
  );
  $data['feed_loader_node_feed']['table']['join'] = array(
    'feed_loader_item' => array(
      'left_field' => 'fid', 
      'field' => 'fid',
    ),
    'node' => array(
      'left_field' => 'nid', 
      'field' => 'nid',
    ),
  );

  // Item ID table field.
  $data['feed_loader_item']['iid'] = array(
    'title' => t('Feed Item ID'), 
    'help' => t('Feed Item content that references a node.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric', 
      'click sortable' => TRUE,
    ), 
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ), 
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  
  // Node ID table field.
  $data['feed_loader_node_feed']['nid'] = array(
    'title' => t('Feed NID'), 
    'help' => t('Feeds content that references a node.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric', 
      'click sortable' => TRUE,
    ), 
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ), 
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
      'relationship' => array(
      'base' => 'node', // The name of the table to join with. 
      'base field' => 'nid', // The name of the field on the joined table.
      'field' => 'nid', // see hook_views_data_alter(); not needed here. 
      'handler' => 'views_handler_relationship', 
      'label' => t('Node'), 
      'title' => t('Node'), 
      'help' => t('Node Related to thie Feed'),
    ),
  );
  
  
  $data['feed_loader_node_feed']['fid'] = array(
    'title' => t('FID'), 
    'help' => t('The Feed ID of the Item.'),
    'relationship' => array(
      'base' => 'feed_loader_item', // The name of the table to join with. 
      'base field' => 'fid', // The name of the field on the joined table.
      'field' => 'fid',//see hook_views_data_alter(); not needed here. 
      'handler' => 'views_handler_relationship', 
      'label' => t('Feeds Item'), 
      'title' => t('Feed Related to this Item'), 
      'help' => t('The Items are related to a Feed'),
    ),
  );
  
  // Example plain text field.
  $data['feed_loader_item']['title'] = array(
    'title' => t('Title'), 
    'help' => t('Feed Title.'), 
    'field' => array(
      'handler' => 'views_handler_field', 
      'click sortable' => TRUE, // This is use by the table display plugin.
    ), 
    'sort' => array(
      'handler' => 'views_handler_sort',
    ), 
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ), 
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // Example plain text field.
  $data['feed_loader_item']['origin'] = array(
    'title' => t('Origin'), 
    'help' => t('Feed Origin.'), 
    'field' => array(
      'handler' => 'views_handler_field', 
      'click sortable' => TRUE, // This is use by the table display plugin.
    ), 
    'sort' => array(
      'handler' => 'views_handler_sort',
    ), 
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ), 
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
    
  // Example numeric text field.
  $data['feed_loader_item']['fid'] = array(
    'title' => t('FID'), 
    'help' => t('Feed ID field.'), 
    'field' => array(
      'handler' => 'views_handler_field_numeric', 
      'click sortable' => TRUE,
    ), 
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ), 
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  
  // Example text field.
  $data['feed_loader_item']['description'] = array(
    'title' => t('Description'), 
    'help' => t('Feed Description.'), 
    'field' => array(
      'handler' => 'views_handler_field', 
      'click sortable' => FALSE, // This is use by the table display plugin.
    ), 
    'sort' => array(
      'handler' => 'views_handler_sort',
    ), 
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ), 
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // Example text field.
  $data['feed_loader_item']['description_html'] = array(
    'title' => t('Body'), 
    'help' => t('Feed Body.'), 
    'field' => array(
      'handler' => 'views_handler_field', 
      'click sortable' => FALSE, // This is use by the table display plugin.
    ), 
    'sort' => array(
      'handler' => 'views_handler_sort',
    ), 
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ), 
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // Example text field.
  $data['feed_loader_item']['image'] = array(
    'title' => t('Feed Image'), 
    'help' => t('Feed Image URL.'), 
    'field' => array(
      'handler' => 'views_handler_field', 
      'click sortable' => FALSE, // This is use by the table display plugin.
    ), 
    'sort' => array(
      'handler' => 'views_handler_sort',
    ), 
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ), 
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  
    // Example text field.
  $data['feed_loader_item']['link'] = array(
    'title' => t('Feed Link'), 
    'help' => t('Feed Original Article URL.'), 
    'field' => array(
      'handler' => 'views_handler_field', 
      'click sortable' => FALSE, // This is use by the table display plugin.
    ), 
    'sort' => array(
      'handler' => 'views_handler_sort',
    ), 
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ), 
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  
  // Example timestamp field.
  $data['feed_loader_item']['timestamp'] = array(
    'title' => t('Timestamp'), 
    'help' => t('The time the Feed was fetched.'), 
    'field' => array(
      'handler' => 'views_handler_field_date', 
      'click sortable' => TRUE,
    ), 
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ), 
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  return $data;
}

/*
 * Implement hook_views_default_views()
 * Abdul@drupalmate.com
 * This provides the Feed View
 */
 
function feed_loader_views_default_views() {

  $views = array();
  $view = new view();
  $view->name = 'feeds';
  $view->description = 'The Feeds View Created from the DrupalMate Feeds Loader Module.';
  $view->tag = 'default';
  $view->base_table = 'feed_loader_node_feed';
  $view->human_name = 'Feeds';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = '<none>';
  $handler->display->display_options['use_ajax'] = TRUE;
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'better_exposed_filters';
  $handler->display->display_options['exposed_form']['options']['submit_button'] = 'Filter';
  $handler->display->display_options['exposed_form']['options']['bef'] = array(
    'origin' => array(
      'more_options' => array(
        'bef_filter_description' => '',
      ),
    ),
  );
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Relationship: Feeds: Node */
  $handler->display->display_options['relationships']['nid']['id'] = 'nid';
  $handler->display->display_options['relationships']['nid']['table'] = 'feed_loader_node_feed';
  $handler->display->display_options['relationships']['nid']['field'] = 'nid';
  /* Relationship: Feeds: Feed Related to this Item */
  $handler->display->display_options['relationships']['fid']['id'] = 'fid';
  $handler->display->display_options['relationships']['fid']['table'] = 'feed_loader_node_feed';
  $handler->display->display_options['relationships']['fid']['field'] = 'fid';
  /* Field: Feeds_Items: Feed Link */
  $handler->display->display_options['fields']['link']['id'] = 'link';
  $handler->display->display_options['fields']['link']['table'] = 'feed_loader_item';
  $handler->display->display_options['fields']['link']['field'] = 'link';
  $handler->display->display_options['fields']['link']['relationship'] = 'fid';
  $handler->display->display_options['fields']['link']['label'] = '';
  $handler->display->display_options['fields']['link']['exclude'] = TRUE;
  $handler->display->display_options['fields']['link']['element_label_colon'] = FALSE;
  /* Field: Feeds_Items: Feed Item ID */
  $handler->display->display_options['fields']['iid']['id'] = 'iid';
  $handler->display->display_options['fields']['iid']['table'] = 'feed_loader_item';
  $handler->display->display_options['fields']['iid']['field'] = 'iid';
  $handler->display->display_options['fields']['iid']['relationship'] = 'fid';
  $handler->display->display_options['fields']['iid']['label'] = '';
  $handler->display->display_options['fields']['iid']['exclude'] = TRUE;
  $handler->display->display_options['fields']['iid']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['iid']['separator'] = '';
  /* Field: Feeds_Items: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'feed_loader_item';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['relationship'] = 'fid';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['text'] = '<h1>[title]</h1>';
  $handler->display->display_options['fields']['title']['alter']['path'] = '[link]';
  $handler->display->display_options['fields']['title']['alter']['external'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['target'] = '_blank';
  $handler->display->display_options['fields']['title']['element_type'] = 'h2';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  /* Field: Feeds_Items: Timestamp */
  $handler->display->display_options['fields']['timestamp']['id'] = 'timestamp';
  $handler->display->display_options['fields']['timestamp']['table'] = 'feed_loader_item';
  $handler->display->display_options['fields']['timestamp']['field'] = 'timestamp';
  $handler->display->display_options['fields']['timestamp']['relationship'] = 'fid';
  $handler->display->display_options['fields']['timestamp']['label'] = '';
  $handler->display->display_options['fields']['timestamp']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['timestamp']['date_format'] = 'long';
  /* Field: Feeds_Items: Origin */
  $handler->display->display_options['fields']['origin']['id'] = 'origin';
  $handler->display->display_options['fields']['origin']['table'] = 'feed_loader_item';
  $handler->display->display_options['fields']['origin']['field'] = 'origin';
  $handler->display->display_options['fields']['origin']['relationship'] = 'fid';
  $handler->display->display_options['fields']['origin']['label'] = '';
  $handler->display->display_options['fields']['origin']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['origin']['alter']['path'] = 'http://[origin]';
  $handler->display->display_options['fields']['origin']['alter']['path_case'] = 'ucfirst';
  $handler->display->display_options['fields']['origin']['alter']['target'] = '_blank';
  $handler->display->display_options['fields']['origin']['element_label_colon'] = FALSE;
  /* Field: Feeds_Items: Feed Image */
  $handler->display->display_options['fields']['image']['id'] = 'image';
  $handler->display->display_options['fields']['image']['table'] = 'feed_loader_item';
  $handler->display->display_options['fields']['image']['field'] = 'image';
  $handler->display->display_options['fields']['image']['relationship'] = 'fid';
  $handler->display->display_options['fields']['image']['label'] = '';
  $handler->display->display_options['fields']['image']['element_label_colon'] = FALSE;
  /* Field: Feeds_Items: Description */
  $handler->display->display_options['fields']['description']['id'] = 'description';
  $handler->display->display_options['fields']['description']['table'] = 'feed_loader_item';
  $handler->display->display_options['fields']['description']['field'] = 'description';
  $handler->display->display_options['fields']['description']['relationship'] = 'fid';
  $handler->display->display_options['fields']['description']['label'] = '';
  $handler->display->display_options['fields']['description']['alter']['strip_tags'] = TRUE;
  $handler->display->display_options['fields']['description']['element_label_colon'] = FALSE;
  /* Sort criterion: Feeds_Items: Timestamp */
  $handler->display->display_options['sorts']['timestamp']['id'] = 'timestamp';
  $handler->display->display_options['sorts']['timestamp']['table'] = 'feed_loader_item';
  $handler->display->display_options['sorts']['timestamp']['field'] = 'timestamp';
  $handler->display->display_options['sorts']['timestamp']['relationship'] = 'fid';
  $handler->display->display_options['sorts']['timestamp']['order'] = 'DESC';
  /* Contextual filter: Content: Nid */
  $handler->display->display_options['arguments']['nid']['id'] = 'nid';
  $handler->display->display_options['arguments']['nid']['table'] = 'node';
  $handler->display->display_options['arguments']['nid']['field'] = 'nid';
  $handler->display->display_options['arguments']['nid']['relationship'] = 'nid';
  $handler->display->display_options['arguments']['nid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['nid']['default_argument_type'] = 'node';
  $handler->display->display_options['arguments']['nid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['nid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['nid']['summary_options']['items_per_page'] = '25';
  /* Filter criterion: Feeds_Items: Origin */
  $handler->display->display_options['filters']['origin']['id'] = 'origin';
  $handler->display->display_options['filters']['origin']['table'] = 'feed_loader_item';
  $handler->display->display_options['filters']['origin']['field'] = 'origin';
  $handler->display->display_options['filters']['origin']['relationship'] = 'fid';
  $handler->display->display_options['filters']['origin']['operator'] = 'contains';
  $handler->display->display_options['filters']['origin']['exposed'] = TRUE;
  $handler->display->display_options['filters']['origin']['expose']['operator_id'] = 'origin_op';
  $handler->display->display_options['filters']['origin']['expose']['label'] = 'Origin';
  $handler->display->display_options['filters']['origin']['expose']['operator'] = 'origin_op';
  $handler->display->display_options['filters']['origin']['expose']['identifier'] = 'origin';
  $handler->display->display_options['filters']['origin']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  
  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block_1');
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;


  // (Export ends here.)

  // Add view to list of views to provide.
  $views[$view->name] = $view;

  // ...Repeat all of the above for each view the module should provide.

  // At the end, return array of default views.
  return $views;
}

/**
 * Implementation of hook_views_query_alter
 * @param type $view
 * @param type $query 
 */
/*function feed_loader_views_query_alter(&$view, &$query) {

  //simple example: change the order of the master display
  //if you want to do it only  on a certain display add something 
  // like  "&&$view->current_display == 'panel_page_1'"
  if ($view->name == 'feeds') {
   foreach ($query->where as &$condition_group) {
      foreach ($condition_group['conditions'] as &$condition) {
        if ($condition['field'] == 'feed_loader_item_feed_loader_node_feed.origin') {
          $condition = array(
            'field' => 'feed_loader_item_feed_loader_node_feed.origin', 
            'value' => '%%', 
            'operator' => 'LIKE',
          );
        }
      }
    }
  return $condition;
  }
  
}*/