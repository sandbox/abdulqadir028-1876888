<?php

/**
 * @file
 * Default theme implementation to wrap feed_loader content.
 *
 * Available variables:
 * - $content: All feed_loader content.
 * - $page: Pager links rendered through theme_pager().
 *
 * @see template_preprocess()
 * @see template_preprocess_feed_loader_wrapper()
 *
 * @ingroup themeable
 */
?>
<div id="feed_loader">
  <?php print $content; ?>
  <?php print $pager; ?>
</div>
